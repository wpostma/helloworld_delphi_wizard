unit HelloWizardDataMod;

{ This data module is to show how we would add IDE Insight
  support which requires a TActionList.

  http://stackoverflow.com/questions/10147850/add-my-own-items-to-delphi-ide-insight-f6-with-in-delphi-open-tools-api
  original author: T.Ondrej.

}

interface

uses
  System.SysUtils, System.Classes, System.Actions, Vcl.ActnList,
  System.ImageList, Vcl.ImgList, Vcl.Controls,
  HelloWizardKeyBinding;

type
  THelloWizardDataModule = class(TDataModule)
    WizActions: TActionList;
    ImageList16x16: TImageList;
    ActionHelloWorld: TAction;
    procedure ActionHelloWorldExecute(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FKeyBinding: TKeyboardBinding;
  public
    { Public declarations }
  end;

  { manually init and cleanup from somewhere else with these functions}
procedure DMWizInit;
procedure DMWizCleanup;


var
  HelloWizardDataModule: THelloWizardDataModule;

implementation


uses
  Windows,
  ToolsAPI,
  Dialogs;

// This next bit is for Delphi XE3 and up, and tells Delphi whether this unit has an affinity to the VCL
// or to Firemonkey.
{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure THelloWizardDataModule.ActionHelloWorldExecute(Sender: TObject);
begin
{$ifdef DEBUG}
  OutputDebugString( 'Data Module Action: THelloWizardDataModule.ActionHelloWorldExecute' );
{$endif}
  ShowMessage('Hello World!');
end;

var
  Index:Integer = -1;

procedure THelloWizardDataModule.DataModuleCreate(Sender: TObject);
begin
  { Load any user customizations BEFORE you create the TKeyboardBinding.
    Or go rewrite TKeyboardBinding to make it unbind and rebind whenever you like. Your call.
   }
  FKeyBinding := TKeyboardBinding.Create( WizActions); { This is a TInterfacedObject, it is not freed explicitly }
end;

procedure THelloWizardDataModule.DataModuleDestroy(Sender: TObject);
begin
  //FKeyBinding.Free; // Don't do this. Also Warren wrote the code so you can't do what you shouldn't do. Good idea right?
  FKeyBinding := nil; // This thing is reference counted, freed when reference hits zero.
end;


procedure DMWizInit;
begin
  if not Assigned(HelloWizardDataModule) then
  begin
    HelloWizardDataModule := THelloWizardDataModule.Create(nil); // This isn't a TComponent sadly.
    Index := (BorlandIDEServices as INTAIDEInsightService).AddActionList(HelloWizardDataModule.WizActions);
  end;

end;

procedure DMWizCleanup;
begin
 if Index<> -1 then
    (BorlandIDEServices as INTAIDEInsightService).RemoveActionList(Index);
 Index := -1;
 if Assigned(HelloWizardDataModule) then
 begin
      HelloWizardDataModule.Free;
      HelloWizardDataModule := nil;
 end;
end;



//initialization
//  Index := -1;
//
end.
