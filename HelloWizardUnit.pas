// "Hello World!" for the OpenTools API (IDE versions 4 or greater)
// By Erik Berry: http://www.gexperts.org/, eberry@gexperts.org

unit HelloWizardUnit;

interface

uses ToolsAPI;

type
  // TNotifierObject has stub implementations for the necessary but
  // unused IOTANotifer methods
  THelloWizard = class(TNotifierObject, IOTAMenuWizard, IOTAWizard)
  public
	// IOTAWizard interafce methods(required for all wizards/experts)
	function GetIDString: string;
	function GetName: string;
	function GetState: TWizardState;
	procedure Execute;

  procedure AfterConstruction; override;
  procedure BeforeDestruction; override;

	// IOTAMenuWizard (creates a simple menu item on the help menu)
	function GetMenuText: string;
  end;


implementation

uses HelloWizardDataMod,
    Dialogs;

procedure THelloWizard.AfterConstruction;
begin
  inherited;
  HelloWizardDataMod.DMWizInit; { creates key bindings, IDE Insight actionlist, etc, }
end;

procedure THelloWizard.BeforeDestruction;
begin
  inherited;
  HelloWizardDataMod.DMWizCleanup; { destroys key bindings, actionlist, etc }

end;

procedure THelloWizard.Execute;
begin
  ShowMessage('Hello World!');
end;

function THelloWizard.GetIDString: string;
begin
  Result := 'EB.HelloWizard';
end;

function THelloWizard.GetMenuText: string;
begin
  Result := '&Hello Wizard';
end;

function THelloWizard.GetName: string;
begin
  Result := 'Hello Wizard';
end;

function THelloWizard.GetState: TWizardState;
begin
  Result := [wsEnabled];
end;

end.

