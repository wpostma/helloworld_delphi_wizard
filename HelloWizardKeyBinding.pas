 unit HelloWizardKeyBinding;

 { Wrap up the basics of keyboard bindings in a helper class, designed
   to expose the ActionList's action's keyboard shortcuts automatically.
 }
interface

uses
  Classes,
  SysUtils,
  ToolsAPI,
  Vcl.ActnList,
  ComSvcs;

type
  TKeyboardBinding = Class(TNotifierObject, IOTAKeyboardBinding)
  Private
    FActionList:TActionList;
    FKeyBindingIndex: Integer; // If >0 then a binding must be unbound at destruction

    Procedure Exec(const Context: IOTAKeyContext;
      KeyCode: TShortcut; var BindingResult: TKeyBindingResult);
  Protected
    function GetBindingType: TBindingType;
    function GetDisplayName: string;
    function GetName: string;
    procedure BindKeyboard(const BindingServices: IOTAKeyBindingServices);
  Protected
    procedure Bind; virtual;
  Public
    Free:Integer; // An acceptable public field.

    constructor Create( AnActionList:TActionList);
    destructor Destroy; override;


  End;


implementation

uses
  Menus,
  Dialogs;

{ TKeyboardBinding }


procedure TKeyboardBinding.Bind;
begin
  FKeyBindingIndex := (BorlandIDEServices As IOTAKeyboardServices).AddKeyboardBinding(Self);
end;

procedure TKeyboardBinding.BindKeyboard(
  const BindingServices: IOTAKeyBindingServices);
var
 n:Integer;
 action:TAction;
begin
  { Bind each action that has a shortcut }

  { TextToShortcut('Ctrl+Shift+F8') }
  for n := 0 to FActionList.ActionCount-1 do
  begin
    action := FActionList.Actions[n] as TAction;
    if action.ShortCut<>0 then
    begin
         BindingServices.AddKeyBinding( [action.shortcut], Exec, action, 7);
    end;

  end;
end;

constructor TKeyboardBinding.Create(AnActionList: TActionList);
begin
   FActionList := AnActionList;
   Bind;

end;

destructor TKeyboardBinding.Destroy;
begin
  if FKeyBindingIndex>=0 then
    (BorlandIDEServices As IOTAWizardServices).RemoveWizard(FKeyBindingIndex);
  inherited;
end;

procedure TKeyboardBinding.Exec(const Context: IOTAKeyContext;
  KeyCode: TShortcut; var BindingResult: TKeyBindingResult);
var
  Action: TAction;
begin
  if TObject(context.Context) is TAction then
  begin
      action :=TAction(context.Context);
      if action.Enabled  then
        if action.Execute then
          BindingResult := krHandled;
  end;
end;


function TKeyboardBinding.GetBindingType: TBindingType;
begin
    Result := btPartial;
end;

function TKeyboardBinding.GetDisplayName: string;
begin
  Result := 'Hello Wizard Key Bindings';
end;


function TKeyboardBinding.GetName: string;
begin
  Result := 'Hello Wizard Bindings';
end;

end.
