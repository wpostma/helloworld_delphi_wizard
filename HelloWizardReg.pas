// "Hello World!" for the OpenTools API (IDE versions 4 or greater)
// By Erik Berry: http://www.gexperts.org/, eberry@gexperts.org
unit HelloWizardReg;

interface

uses ToolsAPI,HelloWizardUnit;

procedure Register;

implementation

procedure Register;
begin
  RegisterPackageWizard(THelloWizard.Create);
end;

end.
